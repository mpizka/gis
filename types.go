package main

import (
    "bytes"
    "fmt"
    "strings"
    "time"
    "gitlab.com/mpizka/tctl"
)


// Project is a development project
type Project struct {
    Name    string     // project name
    Tags    []string   // tags used in project
    Users   []User     // users of the project
    Issues  []Issue    // issues of the project
}

// updateTags adds tags to a Project if they are not present
func (p *Project) UpdateTags(t []string) {
    m := make(map[string]bool)
    for _, tag := range p.Tags {
        m[tag] = true
    }
    for _, tag := range t {
        if ! m[tag]{
            p.Tags = append(p.Tags, tag)
        }
    }
}

// findByTitle finds issues by left-match title
func (p *Project) findByTitle(t string) []*Issue {
    r := []*Issue{}
    for i, iss := range p.Issues {
        if strings.Contains(iss.Title, t) {
            r = append(r, &p.Issues[i])
        }
    }
    return r
}

// findByID finds issues by left-matching IDs
func (p *Project) findByID(id string) []*Issue {
    r := []*Issue{}
    for i, iss := range p.Issues {
        if strings.HasPrefix(iss.ID, id) {
            r = append(r, &p.Issues[i])
        }
    }
    return r
}

// findByMark find issue by exact-matching marks
func (p *Project) findByMark(mk string) *Issue {
    for i, iss := range p.Issues {
        if iss.Mark == mk {
            return &p.Issues[i]
        }
    }
    return nil
}


// Issue is an issue within a Project
type Issue struct {
    ID          string
    Opened      time.Time
    OpUser      string
    Closed      time.Time
    ClUser      string
    Title       string
    Mark        string
    Desc        string
    Tags        []string
    Comments    []Comment
}

// Issue.String returns an Issue as string
func (i *Issue) String() string {
    b := bytes.NewBuffer([]byte{})
    b.WriteString(fmt.Sprintf("%s\n\n", i.Title))
    b.WriteString(fmt.Sprintf("opened: %s (%s)\n", i.Opened.Local().Format(isoTimeFmt), i.OpUser))
    if !i.Closed.IsZero() {
        b.WriteString(fmt.Sprintf("closed: %s (%s)\n", i.Closed.Local().Format(isoTimeFmt), i.ClUser))
    }
    b.WriteString(fmt.Sprintf("\n%s\n\n", i.Desc))
    for _, tag := range i.Tags {
        b.WriteString(fmt.Sprintf("%s\n", tag))
    }
    for _, c := range i.Comments {
        b.WriteString(fmt.Sprintf("\n--\n%s", &c))
    }
    return b.String()
}

// Issue.AsString returns Issue as a pretty-printed string
func (i *Issue) PrettyString() string {
    b := bytes.NewBuffer([]byte{})
    b.WriteString(fmt.Sprintf("%s%s%s%s\n\n", tctl.BOLD, tctl.FGREEN, i.Title, tctl.NORMAL))
    b.WriteString(fmt.Sprintf("%sopened: %s (%s)%s\n", tctl.FGREEN, i.Opened.Local().Format(isoTimeFmt), i.OpUser, tctl.NORMAL))
    if !i.Closed.IsZero() {
        b.WriteString(fmt.Sprintf("%sclosed: %s (%s)%s\n", tctl.FGREEN, i.Closed.Local().Format(isoTimeFmt), i.ClUser, tctl.NORMAL))
    }
    b.WriteString(fmt.Sprintf("\n%s\n\n", i.Desc))
    for _, tag := range i.Tags {
        b.WriteString(fmt.Sprintf("%s\n", tag))
    }
    for _, c := range i.Comments {
        b.WriteString(fmt.Sprintf("\n\n%s", c.PrettyString()))
    }
    return b.String()
}


// Issue.HasAllTags returns true if it has all tags
func (i *Issue) HasAllTags(tags []string) bool {
    for _, t := range tags {
        for _, z := range i.Tags {
            if t == z {
                goto Out
            }
        }
        return false
        Out:
    }
    return true
}

// Issue.UpdateTags updates an issues taglist with tags
func (i *Issue) UpdateTags(tags []string) {
    set := make(map[string]bool)
    for _, t := range i.Tags {
        set[t] = true
    }
    for _, t := range tags {
        if !set[t] {
            i.Tags = append(i.Tags, t)
        }
    }
}

// Issue.RemoveTags removes tags from an issues taglist
func (i *Issue) RemoveTags(tags []string) {
    newTags := []string{}
    set := make(map[string]bool)
    for _, t := range tags {
        set[t] = true
    }
    for _, t := range i.Tags {
        if !set[t] {
            newTags = append(newTags, t)
        }
    }
    i.Tags = newTags
}


// IssueListRep is the list-representation of an issue
type IssueListRep struct {
    Mark        string
    ID          string
    OpUser      string
    Title       string
    AgeDays     float64
}

// IssueListRep.String implements Stringer interface for IssueListRep
func (i *IssueListRep) String() string {
    if i.Mark != "" {
        return fmt.Sprintf("%s\t%s\t%.1fd\t%s\t(%s)", i.ID, i.OpUser, i.AgeDays, i.Title, i.Mark )
    }
    return fmt.Sprintf("%s\t%s\t%.1fd\t%s", i.ID, i.OpUser, i.AgeDays, i.Title)
}

// IssueListRep.PrettyString returns IssueListRep as pretty-printed string
func (i *IssueListRep) PrettyString() string {
    if i.Mark != "" {
        return fmt.Sprintf("%s%s\t%s\t%.1fd\t%s\t(%s)%s", tctl.BOLD, i.ID, i.OpUser, i.AgeDays, i.Title, i.Mark, tctl.NORMAL)
    }
    return fmt.Sprintf("%s\t%s\t%.1fd\t%s", i.ID, i.OpUser, i.AgeDays, i.Title)
}


// Comment is a comment on an issue
type Comment struct {
    ID          string
    Created     time.Time
    User        string
    Text        string
}

// Comment.String returns a comment as string
func (c *Comment) String() string {
    b := bytes.NewBuffer([]byte{})
    b.WriteString(fmt.Sprintf("%s (%s)\n%s", c.Created.Local().Format(isoTimeFmt), c.User, c.Text))
    return b.String()
}

// Comment.PrettyString returns comment as pretty-printed string
func (c *Comment) PrettyString() string {
    b := bytes.NewBuffer([]byte{})
    b.WriteString(fmt.Sprintf("%s%s (%s)%s\n%s", tctl.FGREEN, c.Created.Local().Format(isoTimeFmt), c.User, tctl.NORMAL, c.Text))
    return b.String()
}

// User is a User in a dev. project (can edit issues)
type User struct {
    Name    string      // user name
    Pwd     string      // password stored as salted hash
}
