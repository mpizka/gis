// Includes the "doer" commands which perform CRUD actions on project - data.
// These funcs are called by the CLI command functions, and by the SAH (single
// access handler) in server-mode.
//
// While these funcs can manipulate the project, they don't do disk read/write.
// Disk access is handled only by the CLI cmd funcs and the SAH.
package main

import (
    "errors"
    "fmt"
    "strconv"
    "strings"
    //"text/tabwriter"
    "time"
)


// helper to doXYZ funcs: find unique issue by id, title, mark
func findUnique(p *Project, id, title, mk string) (*Issue, error) {
    var qRes []*Issue
    switch {
    case title != "":
        qRes = p.findByTitle(title)
    case id != "":
        qRes = p.findByID(id)
    case mk != "":
        iss := p.findByMark(mk)
        if iss != nil {
            qRes = append(qRes, p.findByMark(mk))
        }
    }

    if len(qRes) != 1 {
        return nil, errors.New("no unambiguous hit")
    }

    return qRes[0], nil
}


// doShow implements "showing" an issue, that is find unique by title or id
func doShow(p *Project, fl *Flgs) (*Issue, error) {
    // find
    iss, err := findUnique(p, fl.ID, fl.Title, fl.Mark)
    if err != nil {
        return nil, fmt.Errorf("show: %s", err)
    }

    return iss, nil
}


// doCreate implements creating issues in a project
// it returns the ID and Title of the new issue
func doCreate(p* Project, fl *Flgs, c map[string]string) (string, error) {
    if fl.Title == "" {
        return "", errors.New("create: missing issue title")
    }

    // update project tags
    p.UpdateTags(fl.Tags)

    // create and save issue
    id := makeId(p)
    p.Issues = append(p.Issues, Issue{
        ID: id,
        Opened: time.Now().UTC(),
        OpUser: c[CFG_USRNAME],
        Title: fl.Title,
        Desc: fl.Msg,
        Tags: fl.Tags,
    })

    return fmt.Sprintf("gis: created issue %s '%s'", fl.ID, fl.Title), nil
}


// doClose implements closing issues in a project
func doClose(p* Project, fl *Flgs, c map[string]string) (string, error) {
    // find
    iss, err := findUnique(p, fl.ID, fl.Title, fl.Mark)
    if err != nil {
        return "", fmt.Errorf("close: %s", err)
    }

    // chk closed already
    if ! iss.Closed.IsZero() {
        return "", errors.New("close: issue already closed")
    }

    iss.Closed = time.Now().UTC()
    iss.ClUser = c[CFG_USRNAME]

    return fmt.Sprintf("gis: closed issue %s", iss.ID), nil
}


// doList implements listing issues of a project
func doList(p* Project, fl *Flgs) ([]IssueListRep, error) {
    list := []IssueListRep{}
    for _, iss := range p.Issues {

        // include/exclude closed issues
        if !fl.Incl && !iss.Closed.IsZero() {
            continue
        }
        // filter by opening user
        if fl.Usr != "" && !(fl.Usr == iss.OpUser) {
            continue
        }
        // filter by title substring
        if ! strings.Contains(iss.Title, fl.Title) {
            continue
        }
        // filter by tags
        if ! iss.HasAllTags(fl.Tags) {
            continue
        }
        // filter by from & to
        if ! fl.From.IsZero() && fl.From.After(iss.Opened.UTC()) {
            continue
        }
        if ! fl.To.IsZero() && iss.Opened.UTC().After(fl.To) {
            continue
        }
        ageDays := time.Since(iss.Opened).Hours() / 24.0
        list = append(list, IssueListRep{iss.Mark, iss.ID, iss.OpUser, iss.Title, ageDays})
    }
    return list, nil
}


// doComment implements commenting on issues of a project
func doComment(p* Project, fl *Flgs, c map[string]string) (string, error) {
    // find
    iss, err := findUnique(p, fl.ID, fl.Title, fl.Mark)
    if err != nil {
        return "", fmt.Errorf("comment: %s", err)
    }

    // mk comment id
    commentID := strconv.Itoa(len(iss.Comments))

    // add comment to Issue
    comment := Comment{
        ID: commentID,
        Created: time.Now().UTC(),
        User: c[CFG_USRNAME],
        Text: fl.Msg,
    }
    iss.Comments = append(iss.Comments, comment)
    return fmt.Sprintf("gis: comment appended to %s", iss.ID), nil
}


// doTag implements tagging an issue
func doTag(p *Project, fl *Flgs) (string, error) {
    // find
    iss, err := findUnique(p, fl.ID, fl.Title, fl.Mark)
    if err != nil {
        return "", fmt.Errorf("tag: %s", err)
    }

    iss.UpdateTags(fl.Tags)
    p.UpdateTags(fl.Tags)
    return fmt.Sprintf("gis: updated tags"), nil
}


// doUntag implements removing tags from an issue
func doUntag(p *Project, fl *Flgs) (string, error) {
    // find
    iss, err := findUnique(p, fl.ID, fl.Title, fl.Mark)
    if err != nil {
        return "", fmt.Errorf("untag: %s", err)
    }

    iss.RemoveTags(fl.Tags)
    return fmt.Sprintf("gis: updated tags"), nil
}


// doMark implements marking issues
func doMark(p *Project, fl *Flgs) (string, error) {
    // find (not by mark)
    iss, err := findUnique(p, fl.ID, fl.Title, "")
    if err != nil {
        return "", fmt.Errorf("mark: %s", err)
    }

    // check for mark
    if fl.Mark == "" {
        return "", errors.New("mark: no -mk given")
    }

    // check if mark is set on issue already
    marked := p.findByMark(fl.Mark)
    if marked != nil {
        marked.Mark = ""
        iss.Mark = fl.Mark
        return fmt.Sprintf("mark: switched mark (%s) from %s -> %s", fl.Mark, marked.ID, iss.ID), nil
    }

    // set new mark
    iss.Mark = fl.Mark
    return fmt.Sprintf("mark: issue %s marked (%s)", iss.ID, fl.Mark), nil
}


//doUnmark implements removing the mark from issues
func doUnmark(p *Project, fl *Flgs) (string, error) {
    // find
    iss, err := findUnique(p, fl.ID, fl.Title, fl.Mark)
    if err != nil {
        return "", fmt.Errorf("unmark: %s", err)
    }

    // unmark
    iss.Mark = ""
    return fmt.Sprintf("unmark: %s unmarked", iss.ID), nil
}
