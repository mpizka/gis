# gis

Go Issue - Lightweight issue-tracking for software development


# project state

**Early Development**

Tag `v0.1` is a stable version including the basic functionality
(init, create, list, show, comment, tag, untag). Documentation is missing
as is server functionality.


# project goals

Writing a simple, lightweight, easy-to-use issue tracker software, catering
to the needs of devlopers. The documentation should fit in a short textfile
and be digestible in less than 15 minutes.

The software should double as a command line tool and as a server-application
offering communal issue tracking to teams of developers.

It should avoid feature-creep and unecessary complexity.


# not project goals

## Making a project management tool
No assigning, estimates, work-logging, due dates, graphs or pie-charts.
It doesn't care whether you use Agile, Waterfall, Kanban or what-have-you.
It  tracks issues for you, end of story.

## Making it ''User-friendly''
should provide a command-line-tool which can double as a server.
Commands are meant to be short & simple, for people familiar with CLI.
There will be no built-in GUI or Web-frontend.

## Making it ''Feature-rich''
Unix programming philosophy: "Do one thing and do it well".
The one thing is to track issues in software-development projects, period.
