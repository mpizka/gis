// gis (Go Issue) is a lightweight issue tracking tool for software-development projects
package main

import (
    "encoding/json"
    "errors"
    "flag"
    "fmt"
    "compress/gzip"
    "io"
    "io/ioutil"
    "log"
    "os"
    "runtime"
    "strings"
    "time"
    "golang.org/x/crypto/ssh/terminal"
    "gitlab.com/mpizka/getfromeditor"
)


// time formats
const (
    isoTimeFmt = "2006-01-02 15:04:05"
    isoTimeShortFmt = "2006-01-02"
)

// user config map keys
const (
    CFG_USRNAME = "UsrName"
    CFG_DEFAULTMARK = "DefaultMark"
)


// Flgs is the command line flags used by gis
// The struct should be created via getFlgs()
// It can be json.Marshal'ed into a POST body to send to a server
// without the "raw" versions of some values
type Flgs struct {
    File    string      // file
    Title   string      // issue title
    ID      string      // issue ID
    Msg     string      // issue/comment msg (text)
    Usr     string      // user name
    Incl    bool        // include flag (e.g. closed issues in list)
    Name    string      // project name
    Key     string      // key (e.g. for config)
    Val     string      // value for key
    Mark    string      // issue mark
    // parsed values
    rTags   string      // raw tags, whitespace separated
    rFrom   string      // raw time-from
    rTo     string      // raw time-to
    Tags    []string    // tags after parsing    
    From    time.Time   // time-from
    To      time.Time   // time-to
}

// getFlgs reads, parses and returns the command line flags in a form that
// is usable by CLI commands and the http handler
func getFlgs() (*Flgs, error) {
    f := new(Flgs)
    fs := flag.NewFlagSet("gis cli flags", flag.ExitOnError)
    fs.StringVar(&f.File, "f", ".gis", "project file")
    fs.StringVar(&f.Title, "t",  "", "issue title")
    fs.StringVar(&f.ID, "id", "", "id")
    fs.StringVar(&f.Msg, "m", "", "message")
    fs.StringVar(&f.Usr, "u", "", "opening user")
    fs.BoolVar(&f.Incl, "cl", false, "include closed issues")
    fs.StringVar(&f.Name, "n", "local", "project name")
    fs.StringVar(&f.Key, "k", "", "config key to change")
    fs.StringVar(&f.Val, "v", "", "value for config key")
    fs.StringVar(&f.Mark, "mk", "", "issue mark")
    // parsed values: define
    fs.StringVar(&f.rTags, "tg", "", "required tags")
    fs.StringVar(&f.rFrom, "from", "0000-01-01 00:00:00", "timeframe start")
    fs.StringVar(&f.rTo, "to", "9999-01-01 00:00:00", "timeframe end")
    fs.Parse(os.Args[2:])
    // parsed values: parse
    f.Tags = strings.Fields(f.rTags)
    from, err := parseTimeArgs(f.rFrom)
    if err != nil {
        return nil, fmt.Errorf("flags: %v", err)
    }
    f.From = from
    to, err := parseTimeArgs(f.rTo)
    if err != nil {
        return nil, fmt.Errorf("flags: %v", err)
    }
    f.To = to
    return f, nil
}

// parseTimeArgs: helper, converting iso short/long fmt sting -> time.Time
// all times are stored as UTC, but entered as local time by the user
func parseTimeArgs(s string) (time.Time, error) {
    // get local time zone
    loc := time.Now().Location()
    // get user input as locl timestamp
    t, err := time.ParseInLocation(isoTimeShortFmt, s, loc)
    if err != nil {
        t, err = time.ParseInLocation(isoTimeFmt, s, loc)
        if err != nil {
            return t, fmt.Errorf("tparse: cannot parse '%s'", s)
        }
    }
    // return time as UTC for storage
    return t.UTC(), nil
}


// newCfg initializes a new config with default values
func newCfg() map[string]string {
    c := make(map[string]string)
    c[CFG_USRNAME] = "local"
    c[CFG_DEFAULTMARK] = "mine"
    return c
}

// getCfg fetches local user config
func getCfg() (map[string]string, error) {
    path := os.Getenv("HOME") + "/.gis/config.json"
    raw, err := ioutil.ReadFile(path)
    if err != nil {
        return nil, fmt.Errorf("load cfg: %v", err)
    }
    c := make(map[string]string)
    if err := json.Unmarshal(raw, &c); err != nil {
        return nil, fmt.Errorf("load cfg: %v", err)
    }
    return c, nil
}

// putCfg writes a config to the user config file
func putCfg(c map[string]string) error {
    home := os.Getenv("HOME")
    if home == "" {
        return errors.New("cfg write: env $HOME not set")
    }
    path := home + "/.gis/config.json"
    data, err := json.MarshalIndent(c, "", " ")
    if err != nil {
        return fmt.Errorf("cfg write: %v", err)
    }
    if err := ioutil.WriteFile(path, data, 0644); err != nil {
        return fmt.Errorf("cfg write: %v", err)
    }
    return nil
}


// defaultMark sets fl.Mark to the users configured default-Mark if no other
// identifier (Title, ID) is present and Mark wasn't set
func defaultMark(fl *Flgs, c map[string]string) {
    if fl.Mark == "" && fl.Title == "" && fl.ID == "" {
        fl.Mark = c[CFG_DEFAULTMARK]
    }
}


// makeId returns an id string based on sha1 of current time as string
// id is projname-runningnumber-username-4ByteRndHex
func makeId(p *Project) string {
    //runningNumber
    runNr := len(p.Issues) + 1
    return fmt.Sprintf("%d-%s", runNr, p.Name)

}

// getProj fetches project data from gzipped json file
func getProj(path string) (*Project, error) {

    // open file as reader
    fh, err := os.Open(path)
    defer fh.Close()
    if err != nil {
        return nil, fmt.Errorf("load proj: %v", err)
    }

    // open gzip reader using file
    gzr, err := gzip.NewReader(fh)
    defer gzr.Close()
    if err != nil {
        return nil, fmt.Errorf("load proj: %v", err)
    }

    // open json decoder using gzip reader
    dec := json.NewDecoder(gzr)

    // pull data from reader-chain, decode, mk project struct
    var p Project
    for {
        if err := dec.Decode(&p); err ==io.EOF {
            break
        } else if err != nil {
            return nil, fmt.Errorf("load proj: %v", err)
        }
    }

    return &p, nil
}

// putProj writes project data to disk
func putProj(p *Project, path string) error {

    // open file as writer
    fh, err := os.Create(path)
    defer fh.Close()
    if err != nil {
       fmt.Errorf("save proj: %v", err)
    }

    // open gzip writer using file
    gzw := gzip.NewWriter(fh)
    defer gzw.Close()

    // open json encoder using gzip writer
    enc := json.NewEncoder(gzw)

    // push data to writer-chain
    if err := enc.Encode(p); err != nil {
        return fmt.Errorf("save proj: %v", err)
    }

    return nil
}


// getMsg interprets Flgs.Msg 's value (string, editorinvoke, file)
func getMsg(fl *Flgs) error {
    switch {
    // get Msg from editor
    case fl.Msg == "":
        cb, err := getfromeditor.GetFromEditor("", []byte{})
        if err != nil {
            return fmt.Errorf("getMsg: %v", err)
        }
        fl.Msg = string(cb)
    // get Msg from file
    case fl.Msg[0] == '@':
        // TODO TODO TODO I AM HERE TODO TODO TODO
    }
    return nil
}


func main() {
    // set logger flags TODO: remove this when doing change gis-12
    log.SetFlags(0)

    // get cmd
    if len(os.Args) < 2 {
        log.Fatal("gis fatal: no cmd")
    }
    subcmd := os.Args[1]
    // parse CLI arguments
    flgs, err := getFlgs()
    if err != nil {
        log.Fatalf("gis fatal: %s", err)
    }

    switch subcmd {
        case "init":
            err = cmdInit(flgs)
        case "create":
            err = cmdCreate(flgs)
        case "show":
            err = cmdShow(flgs)
        case "close":
            err = cmdClose(flgs)
        case "list":
            err = cmdList(flgs)
        case "comment":
            err = cmdComment(flgs)
        case "tag":
            err = cmdTag(flgs)
        case "untag":
            err = cmdUntag(flgs)
        case "config":
            err = cmdConfig(flgs)
        case "mark":
            err = cmdMark(flgs)
        case "unmark":
            err = cmdUnmark(flgs)
        default:
            log.Fatalf("gis fatal: unknown command '%s'\n", os.Args[1])
    }

    if err != nil {
        log.Fatalf("gis fatal: %s", err)
    }
}



// Subcommand implementation functions
// The following functions implement the subcommands of gis


// cmdInit implements the `gis init` subcommand which inits new project files
// it has no doXYZ to call, as it is only called locally
func cmdInit(fl *Flgs) error {
    // make sure file doesn't exist
    if _, err := os.Stat(fl.File); !os.IsNotExist(err) {
        return errors.New("init: file exists, won't clobber")
    }

    // make empty project
    p := Project{fl.Name, []string{}, []User{}, []Issue{}}
    // write proj to file
    if err := putProj(&p, fl.File); err != nil {
        return fmt.Errorf("init: %v", err)
    }
    fmt.Printf("gis: init project '%s' (%s)\n", fl.Name, fl.File)
    return nil
}


// cmdCreate implements the CLI subcommand create
func cmdCreate(fl *Flgs) error {
    // get local config
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("create: %v", err)
    }

    // get message
    if fl.Msg == "" {
        b, err := getfromeditor.GetFromEditor("", []byte{})
        if err != nil {
            return fmt.Errorf("create: %v", err)
        }
        fl.Msg = string(b)
    }

    // load project file
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("create: %v", err)
    }

    // invoke doXYZ
    resp, err:= doCreate(p, fl, c)
    if err != nil {
        return err
    }

    // write project file
    if err := putProj(p, fl.File); err != nil {
        return fmt.Errorf("create: %s", err)
    }

    fmt.Printf("%s\n", resp)
    return nil
}


// cmdShow implements the CLI subcommand show
func cmdShow(fl *Flgs) error {
    // get local config
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("show: %v", err)
    }

    defaultMark(fl, c)

    // load project file
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("show: %v", err)
    }

    // invoke doXYZ
    iss, err := doShow(p, fl)
    if err != nil {
        return  err
    }

    // print
    if runtime.GOOS != "windows" && terminal.IsTerminal(int(os.Stdout.Fd())) {
        fmt.Printf("%s\n", iss.PrettyString())
    } else {
        fmt.Printf("%s\n", iss)
    }
    return nil
}


// cmdClose implements the CLI subcommand close
func cmdClose(fl *Flgs) error {
    // get local config
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("close: %v", err)
    }

    defaultMark(fl, c)

    // load project file
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("close: %v", err)
    }

    // invoke doXYZ
    resp, err := doClose(p, fl, c)
    if err != nil {
        return err
    }

    // write project file
    if err := putProj(p, fl.File); err != nil {
        return fmt.Errorf("close: %v", err)
    }

    fmt.Printf("%s\n", resp)
    return nil
}


// cmdList implements the CLI subcommand list
func cmdList(fl *Flgs) error {
    // load project file
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("list: %v", err)
    }

    // invoke doXYZ
    resp, err := doList(p, fl)
    if err != nil {
        return err
    }

    // response is []IssueListRep, print it
    if runtime.GOOS != "windows" && terminal.IsTerminal(int(os.Stdout.Fd())) {
        for _, ir := range resp {
            fmt.Printf("%s\n", ir.PrettyString())
        }
    } else {
        for _, ir := range resp {
            fmt.Printf("%s\n", ir.String())
        }
    }
    return nil
}


// cmdComment implements the CLI subcommand comment
func cmdComment(fl *Flgs) error {
    // get local config
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("comment: %v", err)
    }

    defaultMark(fl, c)

    // load project file
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("comment: %v", err)
    }

    // get comment
    if fl.Msg == "" {
        cb, err := getfromeditor.GetFromEditor("", []byte{})
        if err != nil {
            return fmt.Errorf("comment: %v", err)
        }
        fl.Msg = string(cb)
    }

    // invoke doXYZ
    resp, err := doComment(p, fl, c)
    if err != nil {
        return err
    }

    // save project to disk
    if err := putProj(p, fl.File); err != nil {
        return fmt.Errorf("comment: %v", err)
    }
    fmt.Printf("%s\n", resp)
    return nil
}

// cmdTag implements the CLI subcommand tag
func cmdTag(fl *Flgs) error {
    // get local config
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("comment: %v", err)
    }

    defaultMark(fl, c)

    // load project
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("tag: %v", err)
    }

    // invoke doXYZ
    resp, err := doTag(p, fl)
    if err != nil {
        return err
    }

    // write proj to disk
    if err := putProj(p, fl.File); err != nil {
        return  fmt.Errorf("tag: %v", err)
    }

    fmt.Printf("%s\n", resp)
    return nil
}


// cmdUntag implements the CLI subcommand untag
func cmdUntag(fl *Flgs) error {
    // get local config
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("comment: %v", err)
    }

    defaultMark(fl, c)

    // load project
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("untag: %v", err)
    }

    // invoke doXYZ
    resp, err := doUntag(p, fl)
    if err != nil {
        return err
    }

    // write proj to disk
    if err := putProj(p, fl.File); err != nil {
        return  fmt.Errorf("untag: %v", err)
    }

    fmt.Printf("%s\n", resp)
    return nil
}


// cmdMark implements the CLI subcommand mark
func cmdMark(fl *Flgs) error {
    // get local config
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("comment: %v", err)
    }

    // use default Mark
    if fl.Mark == "" {
        fl.Mark = c[CFG_DEFAULTMARK]
    }

    // load project
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("mark: %v", err)
    }

    // invoke doXYZ
    resp, err := doMark(p, fl)
    if err != nil {
        return err
    }

    // write proj to disk
    if err := putProj(p, fl.File); err != nil {
        return  fmt.Errorf("mark: %v", err)
    }

    fmt.Printf("%s\n", resp)
    return nil
}


// cmdUnmark implements the CLI subcommand unmark
func cmdUnmark(fl *Flgs) error {
    // get local config
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("comment: %v", err)
    }

    defaultMark(fl, c)

    // load project
    p, err := getProj(fl.File)
    if err != nil {
        return  fmt.Errorf("unmark: %v", err)
    }

    // invoke doXYZ
    resp, err := doUnmark(p, fl)
    if err != nil {
        return err
    }

    // write proj to disk
    if err := putProj(p, fl.File); err != nil {
        return  fmt.Errorf("unmark: %v", err)
    }

    fmt.Printf("%s\n", resp)
    return nil
}


// cmdConfig implements the `gis config` subcommand
// which edits the users configuration file
// it has no doXYZ to call, as it is only called locally
func cmdConfig(fl *Flgs) error {
    path := os.Getenv("HOME") + "/.gis/config.json"
    var c map[string]string

    // build new cfg
    if _, pErr := os.Stat(path); os.IsNotExist(pErr) {
        c = newCfg()
        putCfg(c)
        fmt.Printf("gis: built new user config in %s\n", path)
        return nil
    }
    // use existing cfg
    c, err := getCfg()
    if err != nil {
        return fmt.Errorf("config: %v", err)
    }

    switch {
    // print current config
    case fl.Key == "" && fl.Val == "":
        fmt.Printf("UsrName: %s\n", c[CFG_USRNAME])
    // change config
    case fl.Key != "" && fl.Val != "":
        switch fl.Key {
            case CFG_USRNAME:
                c[CFG_USRNAME] = fl.Val
            case CFG_DEFAULTMARK:
                c[CFG_DEFAULTMARK] = fl.Val
            default:
                return fmt.Errorf("config: unknown key: %s", fl.Key)
        }
        putCfg(c)
    default:
        return errors.New("config: missing -key or -val")
    }

    return nil
}
